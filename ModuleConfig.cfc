
component  hint="bluCore"{
	property name="settingsService"		inject="id:settingService@cb";
	property name="pageService"			inject="id:pageService@cb";
	property name="CBHelper"			inject="id:CBHelper@cb";
	property name="editorService"		inject="id:editorService@cb";
	property name="mediaService"		inject="id:mediaService@cb";
	property name="LoginTrackerService"	inject="id:LoginTrackerService@cb";
	property name="mailService"			inject="id:mailservice@cbMailservices";
	property name="permissionService"	inject="id:permissionService@cb";
	property name='MessageBox' 			inject='MessageBox@cbMessageBox';
	property name="moduleService"		inject="id:moduleService@cb";
	property name="populator" 			inject="wirebox:populator";
	
	// Module Properties
	this.title 				= "bluCore";
	this.author 			= "Bluewater Business Solutions, LLC";
	this.webURL 			= "http://www.bluewatersolutions.com";
	this.description 		= "bluCore";
	this.version			= "1.0";
	// If true, looks for views in the parent first, if not found, then in the module. Else vice-versa
	this.viewParentLookup 	= true;
	// If true, looks for layouts in the parent first, if not found, then in module. Else vice-versa
	this.layoutParentLookup = true;
	// Module Entry Point
	this.entryPoint			= "bluCore";

	function configure(){

		// parent settings
		parentSettings = {

		};

		// module settings - stored in modules.name.settings
		settings = 
					{
					
					}
					;

		// Layout Settings
		layoutSettings = {
			defaultLayout = ""
		};

		// datasources
		datasources = {
    		contentbox   = {name="contentbox"}
		};

		// web services
		webservices = {

		};

		// SES Routes
		routes = [
			// Module Entry Point
			
			// Convention Route
			{pattern="/:handler/:action?"}
		];

		// Custom Declared Points
		interceptorSettings = {
			customInterceptionPoints = ""
		};

		// Custom Declared Interceptors
		interceptors = [
		];

		// Binder Mappings
		// binder.map( "Alias" ).to( "#moduleMapping#.model.MyService" );

	}


    function checkPermission(){
		var oPermission = permissionService.findByPermission( 'BLURENT_ADMIN2' );
		oPermission = ( isNull( oPermission) ? addPermission( 'BLURENT_ADMIN2','BLURENT_ADMIN2' ) : writeoutput('added') );
    }
    function addPermission(permission,description){
		var q = new Query(sql="insert into cb_permission  set permission = :permission, description=:description, createdDate=:createdDate, modifiedDate=:modifiedDate" );
			q.addParam(name="permission", value=arguments.permission );
			q.addParam(name="description", value=arguments.description );
			
			q.addParam(name="createdDate", value='2016-08-10 17:48:29' );
			q.addParam(name="modifiedDate", value='2016-08-10 17:48:29' );

			q.execute();

    }

function onLoad(){
	//checkPermission();


	var settingService 	= wirebox.getInstance( "SettingService@cb" );



		var prc.settings='Bluewater Business Solutions';
		var args 	= { name="cb_rss_copyright" };
		var setting = settingService.findWhere( criteria=args );

		setting.setValue(  prc.settings  );
		settingService.save( setting );

		var prc.settings='http://www.bluewatersolutions.com/blog/rss/category/latest-news/';
		var args 	= { name="cb_dashboard_newsfeed" };
		var setting = settingService.findWhere( criteria=args );
		setting.setValue(  prc.settings  );
		settingService.save( setting );


		var prc.settings='Dashboard';
		var args 	= { name="cb_dashboard_welcome_title" };
		var setting = settingService.findWhere( criteria=args );
		setting.setValue(  prc.settings  );
		settingService.save( setting );




		


		var menuService = controller.getWireBox().getInstance("AdminMenuService@cb");
		// Add Menu Contribution
		//menuService.addTopMenu(name = "blusol", label = "Bluewater Solutions", href = "");
		menuService.addTopMenu(name 	= "bluWeb",
							 	label 	= "<i class='fa fa-globe'></i> Website Maintenance",
							 	href 	= "");

		menuService.addSubMenu(topMenu = "SYSTEM", name="bluCoreSettings", label="<i class='fa fa-wrench'></i> bluCoreSettings", href="/cbadmin/module/bluCore/settings");



		myFile = expandPath( "./modules/contentbox/modules_user/bluCore/includes/logo.png" );
		myCopyFile = expandPath( "./modules/contentbox-admin/includes/images/ContentBox_90.png" );
		fileCopy( myFile, myCopyFile);


		myFile = expandPath( "./modules/contentbox/modules_user/bluCore/includes/_tags/needhelp.cfm" );
		myCopyFile = expandPath( "./modules/contentbox-admin/views/_tags/needhelp.cfm" );
		fileCopy( myFile, myCopyFile);


		myFile = expandPath( "./modules/contentbox/modules_user/bluCore/includes/_tags/footer.cfm" );
		myCopyFile = expandPath( "./modules/contentbox-admin/views/_tags/footer.cfm" );
		fileCopy( myFile, myCopyFile);

		myFile = expandPath( "./modules/contentbox/modules_user/bluCore/includes/favicon.ico" );
		myCopyFile = expandPath( "./favicon.ico" );
		fileCopy( myFile, myCopyFile);



		menuService.removeSubMenu(topMenu = "dashboard", name="about");
		menuService.removeTopMenu(topMenu = "stats");
		menuService.removeTopMenu(topMenu = "modules");







	}

	/**
	* Fired when the module is unregistered and unloaded
	*/
	function onUnload(){
		var menuService = controller.getWireBox().getInstance("AdminMenuService@cb");
		menuService.removeTopMenu(topMenu = "bluCore");
		menuService.removeSubMenu(topMenu = "SYSTEM", name="bluCoreSettings");
		menuService.removeTopMenu(topMenu = "bluWeb");

	}

	

}