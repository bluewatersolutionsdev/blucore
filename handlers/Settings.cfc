/**
* ContentBox - A Modular Content Platform
* Copyright since 2012 by Ortus Solutions, Corp
* www.ortussolutions.com/products/contentbox
* ---
* Manage system settings
*/
component {

	// Dependencies
	property name="settingsService"		inject="id:settingService@cb";
	property name="pageService"			inject="id:pageService@cb";
	property name="CBHelper"			inject="id:CBHelper@cb";
	property name="editorService"		inject="id:editorService@cb";
	property name="mediaService"		inject="id:mediaService@cb";
	property name="LoginTrackerService"	inject="id:LoginTrackerService@cb";
	property name="mailService"			inject="id:mailservice@cbMailservices";
	property name="permissionService"	inject="id:permissionService@cb";
	property name='MessageBox' inject='MessageBox@cbMessageBox';
	property name="moduleService"	inject="id:moduleService@cb";
	property name="populator" 	inject="wirebox:populator";
	

    defaultEventRedirect = "cbadmin/dashboard";
    eventPermissions = {
        "index": {
            "permissions"     : "SYSTEM_SAVE_CONFIGURATION",
            "message"     : "You do not have permision to view this."   ,
            "nextEvent"       : "cbadmin/dashboard"
        }
    };

    function checkEventPermissions( event, rc, prc ){
        var currentAction = event.getMemento().context.moduleAction;
        if( structKeyExists( eventPermissions, currentAction ) ){
            if( !prc.oAuthor.checkPermission( eventPermissions[ currentAction ][ "permissions" ] ) ){
                if( structKeyExists( eventPermissions[ currentAction ], "permissions" ) ){
                     messagebox.error( eventPermissions[ currentAction ][ "message" ] );   
                }
                if( structKeyExists( eventPermissions[ currentAction ], "nextEvent" ) ){
                    setNextEvent( eventPermissions[ currentAction ][ "nextEvent" ] ); 
                } else {
                    setNextEvent( defaultEventRedirect );
                }
            }   
        }
    }


defaultModuleSettings={
						"blurent_settings_areas": "davis",
						"blurent_settings_locations": "davis",
						"blurent_settings_andrew2": "scott davis",
					}


function checkDefaults( event, rc, prc )
	{
    	this.allsettings=settingsService.getAllSettings();
    	for (key in defaultModuleSettings)
	    	{
	    		if (listfindnocase(valueList(this.allsettings.name), key) is 0)
		    		{
		    			
		    			messagebox.error( key & ' does not exist, we will create it for you'); 
		    			
		    			if (structKeyExists(defaultModuleSettings, key))
			    			{
			    				checkSetting = new Query();
			    				checkSetting.setSql("select settingID from cb_setting where name='#key#'");
			    				checkSettingResult=checkSetting.execute();
			    				if (checkSettingResult.getResult().recordCount is 0)
				    				{
					    				addSetting = new Query();
					    				addSetting.setSql("insert into cb_setting set 
					    						createdDate=now(),
					    						modifiedDate=now(),
					    						isDeleted=0,
					    						name='#key#',
					    						value='#defaultModuleSettings[key]#',
					    						iscore=0");
					    				settingresults=addSetting.execute();
				    				}
			    			}
		    		}
	    		
	    	}
    	
    }

	/**
	* Pre handler
	*/
	function preHandler( event, rc, prc, action, eventArguments ){
		//checkDefaults( event, rc, prc, defaultModuleSettings);
		checkEventPermissions( event, rc, prc, eventPermissions, defaultEventRedirect );
		
		
	}

	/**
	* Settings manager
	* @return html
	*/
	function index( event, rc, prc ){
		// Exit Handler
		prc.xehSaveSettings = "#prc.cbAdminEntryPoint#.settings.save";
		prc.xehEmailTest	= "#prc.cbAdminEntryPoint#.settings.emailTest";
		// pages
		prc.pages = pageService.search( sortOrder="slug asc", isPublished=true ).pages;
		// Get All registered editors so we can display them
		prc.editors = editorService.getRegisteredEditorsMap();
		// Get All registered markups so we can display them
		prc.markups = editorService.getRegisteredMarkups();
		// Get all registered media providers so we can display them
		prc.mediaProviders = mediaService.getRegisteredProvidersMap();
		// tab
		prc.tabSystem_Settings = true;
		// cb helper
		prc.cb = CBHelper;
		// caches
		prc.cacheNames = cachebox.getCacheNames();
		// view

		var modules = moduleService.findModules();
		prc.modules = modules.modules;
		event.setView( "settings/index" );
	}
	
	/**
	* Email Testing of settings
	* @return json
	*/
	function emailTest( event, rc, prc ){
		var mail = mailservice.newMail(
			to			= rc.cb_site_outgoingEmail,
			from		= rc.cb_site_outgoingEmail,
			subject		= "ContentBox Test",
			server		= rc.cb_site_mail_server,
			username	= rc.cb_site_mail_username,
			password	= rc.cb_site_mail_password,
			port		= rc.cb_site_mail_smtp,
			useTLS		= rc.cb_site_mail_tls,
			useSSL		= rc.cb_site_mail_ssl,
			body		= 'Test Email From ContentBox'
		);
		// send it out
		var results = mailService.send( mail );
		
		event.renderData( data=results, type="json" );		
	}

	/**
	* Save settings
	*/
	function save( event, rc, prc ){
		// announce event
		//announceInterception( "cbadmin_preSettingsSave",{ oldSettings = prc.cbSettings, newSettings = rc } );
		// bulk save the options
		settingsService.bulkSave( rc );
		
		// announce event
		//announceInterception( "cbadmin_postSettingsSave" );
		// relocate back to editor
		cbMessagebox.info( "All bluRent settings updated!" );
		setNextEvent(prc.xehSettings);
	}

	


	/**
	* Flush settings cache
	*/
	function flushCache( event, rc, prc ){
		var data = { error = false, messages = "" };
		try{
			settingsService.flushSettingsCache();
			data.messages = "Settings cache flushed!";
		}
		catch(Any e){
			data["ERROR"] = true;
			data["MESSAGES"] = "Error executing flush, please check logs: #e.message#";
			log.error( e.message & e.detail, e );
		}
		event.renderData(data=data, type="json" );
	}

	/**
	* Flush WireBox singletons
	*/
	function flushSingletons( event, rc, prc ){
		wirebox.clearSingletons();
		cbMessagebox.setMessage( "info","All singletons flushed and awaiting re-creation." );
		setNextEvent(event=prc.xehRawSettings,queryString="##wirebox" );
	}

	/**
	* View settings cached data
	* @return html
	*/
	function viewCached( event, rc, prc ){
		var cache 		= settingsService.getSettingsCacheProvider();
		var cacheKey 	= settingsService.getSettingsCacheKey();
		// get Cached Settings
		prc.settings 	= cache.get( cacheKey );
		prc.metadata 	= cache.getCachedObjectMetadata( cacheKey );

		event.setView( view="settings/viewCached", layout="ajax" );
	}



	/**
	* truncate all auth logs
	*/
	any function truncateAuthLogs( event, rc, prc ){
		loginTrackerService.truncate();
		setNextEvent( "#prc.cbAdminEntryPoint#.settings.authLogs" );
	}

}