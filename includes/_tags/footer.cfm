<cfoutput>
<footer id="footer" class="clearfix hidden-phone padding10">
	<!--- cbadmin event --->
	#announceInterception( "cbadmin_loginFooter" )#
	<div class="footer-content text-center">
    	Copyright (C) #dateformat(now(),"yyyy" )# 
    	<a href="http://www.bluewatersolutions.com">Bluewater Business Solutions</a>.<br/>
    	<a href="http://www.bluewatersolutions.com">Need Professional Support, Architecture, Design, or Development?</a>
    </div>
</footer>
</cfoutput>