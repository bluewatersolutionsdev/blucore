<cfoutput>
<div class="text-center margin10">
	<a href="http://www.bluewatersolutions.com" target="_blank" title="Bluewater Business Solutions">
		<img class="img-thumbnail" src="#prc.cbroot#/includes/images/ContentBox_90.png" alt="Bluewater Solutions" border="0" />
	</a>
</div>


<p class="well-sm">
	<strong>Bluewater Business Solutions</strong> is the company behind making your business flow.</p> 
<p class="well-sm">Need professional support, architecture analysis,
code reviews, or custom development? 
<a href="mailto:support@bluewatersolutions.com">Contact us</a>, we are here to help!
</p>
<button class="btn btn-primary"><i class="fa fa-phone"></i>
1-850-308-1631
</button>
</cfoutput>