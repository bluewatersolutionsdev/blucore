component{
	property name="settingsService"		inject="id:settingService@cb";
	
	function checkDefaults( event, rc, prc )
	{
    	this.allsettings=settingsService.getAllSettings();
    	for (key in defaultModuleSettings)
	    	{
	    		if (listfindnocase(valueList(this.allsettings.name), key) is 0)
		    		{
		    			
		    			//messagebox.error( key & ' does not exist, we will create it for you'); 
		    			
		    			if (structKeyExists(defaultModuleSettings, key))
			    			{
			    				checkSetting = new Query();
			    				checkSetting.setSql("select settingID from cb_setting where name='#key#'");
			    				checkSettingResult=checkSetting.execute();
			    				if (checkSettingResult.getResult().recordCount is 0)
				    				{
					    				addSetting = new Query();
					    				addSetting.setSql("insert into cb_setting set 
					    						createdDate=now(),
					    						modifiedDate=now(),
					    						isDeleted=0,
					    						name='#key#',
					    						value='#defaultModuleSettings[key]#',
					    						iscore=0");
					    				settingresults=addSetting.execute();
				    				}
			    			}
		    		}
	    		
	    	}
    	
    }
}