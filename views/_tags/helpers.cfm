<cfoutput>
<!---<script>
$('.logo img').attr('src','http://www.bluewatersolutions.com/__media/logo-small.png');
$('.logo img').attr('width','90');
$('.logo img').attr('height','53');
</script>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/v/bs/b-1.2.1/b-colvis-1.2.1/b-print-1.2.1/se-1.2.0/datatables.min.css"/>
 
<script type="text/javascript" src="//cdn.datatables.net/v/bs/b-1.2.1/b-colvis-1.2.1/b-print-1.2.1/se-1.2.0/datatables.min.js"></script>
--->

<link  href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" type="text/css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<link   href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
<script  src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>

<script type="text/javascript">

    $(document).ready(function(){
      var table = $('.sortable').DataTable({
        fixedHeader: true,
        stateSave: true,
        responsive: true
      });




      $("##propertyForm").validate();


    	$("select").select2();


        $("##myModal").on("show.bs.modal", function(e) {
          var loadingTitle='Loading...'
          var loadingBody='<i class="fa fa-spinner fa-spin fa-1x fa-fw" aria-hidden="true"></i>'
          var link = $(e.relatedTarget);
          $(this).find(".modal-title").html(loadingTitle);
          $(this).find(".modal-body").html(loadingBody);
          $(this).find(".modal-title").html(link.attr("href"));
          $(this).find(".modal-body").load(link.attr("href"));
          
          
        });
        

        $('##btnSave').click(function(event){
            $('##modalForm').submit();
            return false;
        });
        
       
function removeFlag(flagID){
var $flagForm = $( "##flagForm" );
$( "##delete_"+ flagID).removeClass( "fa-trash-o" ).addClass( "fa-spin fa-spinner" );
checkAll( false, 'flagID' );
$flagForm.find( "##flagID" ).val( flagID );
$flagForm.submit();
}


        
	});

$(window).load(function(){




  $('[data-toggle="tabajax"]').click(function(e) {
    var $this = $(this),
        loadurl = $this.attr('href'),
        targ = $this.attr('data-target');

    $.get(loadurl, function(data) {
        $(targ).html(data);
    });

    $this.tab('show');
    return false;
	});
<cfif #structKeyExists(rc,"tab")#>
   $('###rc.tab# [data-toggle="tabajax"]').trigger('click');
   </cfif>

});
</script>
</cfoutput>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width:80%">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">...</h4>
      </div>
      <div class="modal-body">
        <p>Loading...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button name="btnSave" type="button" class="btn btn-danger"  id="btnSave">Save</button>
      </div>
    </div>

  </div>
</div>