﻿<cfoutput>
<div class="row">
    <div class="col-md-12">
        <h1 class="h1">
            <i class="fa fa-wrench fa-lg"></i>
            bluCore Settings
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        #getModel( "messagebox@cbMessagebox" ).renderit()#
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <cfloop  from="1" to="#arraylen(prc.modules)#" index="i">
    <cfif #prc.modules[i].getmemento().isactive#>

    </cfif>
    
           <!--- <cfdump var="#prc.modules[i].gettitle()#">--->
    </cfloop>
   
    
        #html.startForm(name="settingsForm", action=prc.xehSaveSettings, novalidate="novalidate" )#
            #html.anchor(name="top" )#
            <div class="panel panel-default">
                <div class="panel-body">
                    <!--- Vertical Nav --->
                    <div class="tab-wrapper tab-left tab-primary">
                        <!--- Tabs --->
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="##site_options" data-toggle="tab"><i class="fa fa-cog fa-lg"></i> Site Options</a>
                            </li>
                            <cfloop  from="1" to="#arraylen(prc.modules)#" index="i">
                            <cfif #prc.modules[i].getmemento().isactive#>
                            <li>
                                <a href="##settings_#prc.modules[i].getmemento().name#" data-toggle="tab"><i class="fa fa-cog fa-lg"></i> #prc.modules[i].getmemento().title#</a>
                            </li>
                            </cfif>
                            </cfloop>
                            
                            <!--- cbadmin Event --->
                            #announceInterception( "cbadmin_onSettingsNav" )#
                        </ul>
                        <!--- End Tabs --->
                        <!--- Tab Content --->
                        <div class="tab-content">
                            <!--- ********************************************************************* --->
                            <!---                           SITE OPTIONS                                --->
                            <!--- ********************************************************************* --->
                            
                            <div class="tab-pane active" id="site_options">
                               <h2>Settings for bluCore</h2>
                            </div>
                            <cfloop  from="1" to="#arraylen(prc.modules)#" index="i">
                            <cfif #prc.modules[i].getmemento().isactive#>
                            <div class="tab-pane" id="settings_#prc.modules[i].getmemento().name#">
                            <fieldset><legend>#prc.modules[i].getmemento().name#</legend>

                            </fieldset>
                            <cftry>
                           
                            #renderView( "/../../#prc.modules[i].getmemento().name#/views/settings/sections/siteOptions" )#
                           <cfcatch>#cfcatch.message#</cfcatch>
                           </cftry>
                              
                            </div>
                            </cfif>
                            </cfloop>

                           
                            <!--- cbadmin Event --->
                            #announceInterception( "cbadmin_onSettingsContent" )#

                            <!--- Button Bar --->
                            <div class="form-actions">
                                #html.submitButton(value="Save Settings", class="btn btn-danger" )#
                            </div>
                        </div>
                        <!--- End Tab Content --->
                    </div>
                    <!--- End Vertical Nav --->
                </div>
            </div>
        #html.endForm()#
    </div>
</div>
</cfoutput>