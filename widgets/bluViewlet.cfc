/**
* ContentBox - A Modular Content Platform
* Copyright since 2012 by Ortus Solutions, Corp
* www.ortussolutions.com/products/contentbox
* ---
* A widget that executes any internal ColdBox event and return its results
*/
component extends="contentbox.models.ui.BaseWidget" singleton{

	bluViewlet function init(){
		// Widget Properties
		setName( "bluViewlet" );
		setVersion( "1.0" );
		setDescription( "A widget that executes any internal ColdBox event and return its results" );
		setAuthor( "Ortus Solutions" );
		setAuthorURL( "http://www.ortussolutions.com" );
		setCategory( "BluRent" );
		setIcon( 'bed' );
		
		return this;
	}

	/**
	* Execute an internal coldbox event bluviewlet
	* @event.hint The ColdBox event to execute
	* @view.hint The ColdBox view to load
	* @private.hint Private event or not
	* @args.hint Event arguments to pass to the bluviewlet execution, this should be a comma delimitted list of name value pairs. Ex: widget=true,name=Test
	*/
	any function renderIt(required string vevent="bluRentFrontEnd:home.search",boolean private=false,string args="", string view="home/search", string module="bluRentFrontEnd"){
		var rString			= "";
		var eventArguments 	= {};
		
		// Inflate args
		if( len( arguments.args ) ){
			var aString = listToArray( arguments.args, "," );
			
			for( var key in aString ){
				eventArguments['rc'][ listFirst( key, "=" ) ] = getToken( key, 2, "=" );
			}
		}
		
		// generate recent comments
		saveContent variable="rString"{
			// execute it
			
			try{
				writeOutput( runEvent(event=arguments.vevent, eventArguments=eventArguments, private=arguments.private) );
				writeOutput( renderView(view=arguments.view,module=arguments.module) );
			}
			catch(Any e){
				writeOutput( "Error executing bluviewlet: #arguments.vevent#(#arguments.args.toString()#). #e.message#" );
				log.error( "Error executing bluviewlet: #arguments.vevent#(#arguments.args.toString()#)",e);
			}
		}

		return rString;
	}

}
